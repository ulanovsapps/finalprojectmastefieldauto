package driver;


import com.codeborne.selenide.Configuration;
import org.openqa.selenium.chrome.ChromeOptions;

public class DriverInitializer {
    public static void initDriver(BrowserType type) {

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-blink-features=AutomationControlled");
        options.addArguments("--start-fullscreen");

        Configuration.browserCapabilities = options;
        Configuration.timeout = 7000;
        Configuration.browser = "chrome";
        Configuration.pageLoadStrategy = "normal";
        options.addArguments("--headless=new");
//        Configuration.headless = true;
    }
}