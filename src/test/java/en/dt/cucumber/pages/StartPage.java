package en.dt.cucumber.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;


public class StartPage {

    private static final SelenideElement SEARCH_FIELD_ID = $(byXpath("//input[@id='search-input']"));
    private static final SelenideElement LANGUAGE_BTN_ID = $(byId("utility-header-language-switch-link"));
    private static final SelenideElement ACCEPT_COOKIES_BTN = $(byXpath("//*[@id=\"sticky-bar-cookie-wrapper\"]/span/div/div/div[2]/form[1]/button"));
    private static final SelenideElement TEXT_REGISTER = $(byId("ddsweb-cookies-notification-headline"));
    private static final SelenideElement SING_IN_BTN_START_PAGE_ID = $(byXpath("//a[@class='button button-primary']"));
    private static final SelenideElement FAVORITES_ITEM = $(byXpath("//a[@class='nav-item__link nav-item__link--right-aligned' and text()='My Favourites']"));

    public void isLoadRegisterText() {
        TEXT_REGISTER.shouldBe(visible);
    }

    public void clickOnAcceptCookies() {
        ACCEPT_COOKIES_BTN.shouldBe(visible, enabled);
        ACCEPT_COOKIES_BTN.click();
    }

    public String checkTextIsMagyar() {
        LANGUAGE_BTN_ID.shouldBe(enabled);
        return LANGUAGE_BTN_ID.getText();
    }

    public void clickSwapLanguageBtn() {
        LANGUAGE_BTN_ID.click();
    }

    public String checkTextIsEnglish() {
        return LANGUAGE_BTN_ID.getText();
    }

    public void clickLoginBtn() {
        SING_IN_BTN_START_PAGE_ID.shouldBe(enabled);
        SING_IN_BTN_START_PAGE_ID.click();
    }

    public String checkSignInTextOnBtn(String text) {
        SING_IN_BTN_START_PAGE_ID.shouldBe(visible);
        return SING_IN_BTN_START_PAGE_ID.getText();
    }

    public void clickFavoritesBtn() {
        FAVORITES_ITEM.shouldBe(enabled);
        FAVORITES_ITEM.click();
    }

    public void inputTextInSearch(String text) {
        SEARCH_FIELD_ID.shouldBe(enabled);
        SEARCH_FIELD_ID.sendKeys(text);
    }

    public void clickSearch() {
        SEARCH_FIELD_ID.click();
    }

    public void clickSingInBtn() {
        SING_IN_BTN_START_PAGE_ID.click();
    }
}
