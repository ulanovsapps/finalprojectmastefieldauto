package en.dt.cucumber.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FavoritesPage {

    private static final SelenideElement MY_FAVORITES_TEXT_ID = $(byXpath("//h1[text()='My Favourites']"));

    public String checkFavoritesText() {
        MY_FAVORITES_TEXT_ID.shouldBe(visible);
        return MY_FAVORITES_TEXT_ID.getText();
    }
}
