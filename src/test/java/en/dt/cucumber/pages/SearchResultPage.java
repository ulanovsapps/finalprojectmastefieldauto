package en.dt.cucumber.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class SearchResultPage {

    @FindBy(xpath = "//a[@class='nav-item__link nav-item__link--right-aligned' and text()='My Favourites']")
    SelenideElement FILTER_SPAN_ID;

    public void checkFilter() throws InterruptedException {
        Thread.sleep(2000);
        FILTER_SPAN_ID.shouldBe(visible);
    }
}