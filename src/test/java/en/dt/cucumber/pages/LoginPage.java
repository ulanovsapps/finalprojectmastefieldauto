package en.dt.cucumber.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class LoginPage {

    private static final SelenideElement EMAIL_ADDRESS_FIELD_ID = $(byId("email"));
    private static final SelenideElement PASSWORD_FIELD_ID = $(byId("password"));
    private static final SelenideElement SIGN_IN_BUTTON_ID = $(byId("signin-button"));
    private static final SelenideElement LOG_OUT_BTN = $(byId("utility-header-logout-link"));
    private static final SelenideElement RESET_PASSWORD_TEXT_ID = $(byXpath("//h1[text()='Reset Password']"));

    public void inputCredentials(String email, String password) {
        EMAIL_ADDRESS_FIELD_ID.shouldBe(enabled);
        EMAIL_ADDRESS_FIELD_ID.sendKeys(email);
        PASSWORD_FIELD_ID.shouldBe(enabled);
        PASSWORD_FIELD_ID.sendKeys(password);
    }

    public void clickLoginOnLoginPage() {
        SIGN_IN_BUTTON_ID.click();
    }

    public String checkErrorLogin() {
        RESET_PASSWORD_TEXT_ID.shouldBe(visible);
        return RESET_PASSWORD_TEXT_ID.getText();
    }

    public void checkSingOutTextOnBtn() {
        LOG_OUT_BTN.shouldBe(visible, enabled);
    }

    public void clickOnSingOutBtn() {
        LOG_OUT_BTN.click();
    }
}
