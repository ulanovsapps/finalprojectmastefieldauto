package en.dt.cucumber.steps;

import en.dt.cucumber.pages.FavoritesPage;
import en.dt.cucumber.pages.LoginPage;
import en.dt.cucumber.pages.SearchResultPage;
import en.dt.cucumber.pages.StartPage;
import driver.BrowserType;
import driver.DriverInitializer;
import driver.Settings;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Steps {

    private StartPage startPage;
    private LoginPage loginPage;
    private FavoritesPage favoritesPage;
    private SearchResultPage searchResultPage;

    private static final String ERROR_TEXT_LOGIN = "Reset Password";

    @Before
    public void initializeDriver() {
        startPage = new StartPage();
        loginPage = new LoginPage();
        favoritesPage = new FavoritesPage();
        searchResultPage = new SearchResultPage();
        DriverInitializer.initDriver(BrowserType.CHROME_SELMGR);
    }

    @After
    public void closeDriver() {
        getWebDriver().quit();
    }

    @Then("I see Sign out button")
    public void iSeeSignOutButton() {
        loginPage.checkSingOutTextOnBtn();
    }

    @And("I click on Sign out button")
    public void iClickOnSignOutButton() {
        loginPage.clickOnSingOutBtn();
    }

    @Then("I see {string} text")
    public void iSeeText(String text) {
        String actualText = startPage.checkSignInTextOnBtn(text);
        assertEquals(actualText, text);
    }

    @Given("I open the start page")
    public void iOpenTheStartPage() {
        open(Settings.getBaseUrl());
        startPage.isLoadRegisterText();
    }

    @And("I accept cookies on start page")
    public void acceptCookiesOnStartPage() {
        startPage.clickOnAcceptCookies();
    }

    @Given("input text in search field {string}")
    public void inputTextInSearchField(String text) {
        startPage.inputTextInSearch(text);
    }

    @When("click search button")
    public void clickSearchButton() {
        startPage.clickSearch();
    }

    @Given("click on favorites button")
    public void clickOnFavoritesButton() {
        startPage.clickFavoritesBtn();
    }

    @Given("Click on Sing In button")
    public void clickOnSingInButton() {
        startPage.clickSingInBtn();
    }

    @And("I can click login button on start page")
    public void iCanClickLoginButtonOnStartPage() {
        startPage.clickLoginBtn();
    }

    @Given("language is set to english")
    public void languageIsSetToEnglish() {
        String actualText = startPage.checkTextIsMagyar();
        assertEquals(actualText, "Magyar");
    }

    @When("I change the language to hungarian")
    public void iChangeTheLanguageToHungarian() {
        startPage.clickSwapLanguageBtn();
    }

    @Then("Text on button changed to Magyar")
    public void textOnButtonChangedToMagyar() {
        String actualText = startPage.checkTextIsEnglish();
        assertEquals(actualText, "English");
    }

    @And("I input credentials in fields {string} and {string}")
    public void iInputCredentialsInFieldsAnd(String email, String password) {
        loginPage.inputCredentials(email, password);
    }

    @And("I can click login button")
    public void iCanClickLoginButton() {
        loginPage.clickLoginOnLoginPage();
    }

    @Then("I see an Error")
    public void iSeeAnError() {
        String actualText = loginPage.checkErrorLogin();
        assertEquals(actualText, ERROR_TEXT_LOGIN);
    }

    @Then("it shows filter")
    public void itShowsFilter() throws InterruptedException {
        searchResultPage.checkFilter();
    }

    @Then("appears text My Favorites")
    public void appearsTextMyFavorites() {
        String actualText = favoritesPage.checkFavoritesText();
        assertEquals(actualText, "My Favourites");
    }
}
