package without_cucumber;

import driver.Settings;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StartPage {
    private final By acceptCookiesBtn = By.xpath("//*[@id=\"sticky-bar-cookie-wrapper\"]/span/div/div/div[2]/form[1]/button");
    private final By textRegister = By.xpath("//span[text()='Register']");
    private static final By languageBtnId = By.id("utility-header-language-switch-link");

    private final WebDriver driver;
    private final WebDriverWait wait;

    public StartPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    public void iOpenTheStartPage() {
        driver.get(Settings.getBaseUrl());
        WebElement signInBtnElement = driver.findElement(textRegister);
        wait.until(ExpectedConditions.visibilityOf(signInBtnElement));
    }

    public void acceptCookiesOnStartPage() {
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(acceptCookiesBtn))).isEnabled();
        driver.findElement(acceptCookiesBtn).click();
    }

    public void languageIsSetToEnglish() {
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(languageBtnId))).isEnabled();
        String actualText = driver.findElement(languageBtnId).getText();
        assertEquals(actualText, "Magyar");
    }

    public void iChangeTheLanguageToHungarian() {
        driver.findElement(languageBtnId).click();
    }

    public void textOnButtonChangedToMagyar() {
        String actualText = driver.findElement(languageBtnId).getText();
        assertEquals(actualText, "English");
    }
}
