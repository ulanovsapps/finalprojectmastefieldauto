package without_cucumber;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

@Tag("UI")
public class SwapLangTestSuite {

    private static WebDriver driver;
    private static WebDriverWait wait;

    @BeforeAll
    public static void init() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-blink-features=AutomationControlled");
        driver = new ChromeDriver(options);
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }

    @Test
    public void testSwapLanguage() {
        StartPage startPage = new StartPage(driver, wait);
        startPage.iOpenTheStartPage();
        startPage.acceptCookiesOnStartPage();
        startPage.languageIsSetToEnglish();
        startPage.iChangeTheLanguageToHungarian();
        startPage.textOnButtonChangedToMagyar();
    }

    @AfterEach
    public void tearDown() {
        driver.close();
    }
}
