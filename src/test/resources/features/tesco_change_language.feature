#@SmokeTest
Feature: The user shall be able to do change the language in Tesco

  Background:
    Given I open the start page
    And I accept cookies on start page

  Rule: Allow to change the language

    Scenario: Change language
      Given language is set to english
      When I change the language to hungarian
      Then Text on button changed to Magyar

