#@SmokeTest
Feature: The user shall be able to login and logout in Tesco

  Background:
    Given I open the start page
    And I accept cookies on start page
    And I can click login button on start page

  Rule: Allow to login

    Scenario Outline: Open start page, input credentials, login and logout
      Given I input credentials in fields "<login>" and "<password>"
      When I can click login button
      Then I see Sign out button
      And I click on Sign out button
      Then I see "Sign in" text

      Examples:
        | login               | password |
        | isakhorro@gmail.com | a1234567 |
