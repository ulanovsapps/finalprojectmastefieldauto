#@SmokeTest
Feature: The user shall be able to add favorites item in Tesco

  Background:
    Given I open the start page
    And I accept cookies on start page

  Rule: Allow to open favorites and add new item in list

    Scenario Outline: Open start page, input text in search, push button, get result items
      Given I can click login button on start page
      And I input credentials in fields "<login>" and "<password>"
      And I can click login button
      And click on favorites button
      Then appears text My Favorites

      Examples:
        | login               | password |
        | isakhorro@gmail.com | a1234567 |