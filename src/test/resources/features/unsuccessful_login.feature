#@SmokeTest
Feature: The user shall be able search in Tesco

  Background:
    Given I open the start page

  Rule: Must to show error with wrong credential

    Scenario Outline: Open start page, input credentials, login and see the error
      Given Click on Sing In button
      When I input credentials in fields "<login>" and "<password>"
      And I can click login button
      Then I see an Error

      Examples:
        | login         | password |
        | test@test.com | 1234     |